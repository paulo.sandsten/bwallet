import React from 'react'

import './styles/Error.css'

const Error = (props) => (
  <div className='error-container'>
    <h1 className='error-heading'>{props.title}</h1>
    <p className='error-message'>{props.message}</p>
  </div>
)

Error.defaultProps = {
  title: 'Ooops!',
  message: 'Unknown error'
}

export default Error
