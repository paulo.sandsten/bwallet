import React, { useContext, useEffect } from 'react'

// Import components.
import Header from '../components/common/Header'
import Table from '../components/crypto-currency-table/Table'
import Dropdown from '../components/dropdown/Dropdown'
import useFetch from '../utils/UseFetch'

// Styling.
import './styles/Dashboard.css'

// Import context.
import { CryptoCurrenciesContext } from '../context/CryptoCurrencies'

const Dashboard = (props) => {
  const { cryptoCurrencies } = useContext(CryptoCurrenciesContext)
  const { loading } = useContext(CryptoCurrenciesContext)
  const { error } = useContext(CryptoCurrenciesContext)
  const fetchData = useFetch()
  const convert = ['USD', 'BTC','ETH', 'XRP', 'BCH', 'LTC']
  const defaultConvert = 'USD'
  const start = 1
  const limit = 25

  useEffect(() => {
    // TODO: Fix this to be differect for localhost. and in a config file.
    const baseUrl = '/.netlify/functions/cryptocurrencies'
    
    fetchData(`${baseUrl}?start${start}&limit=${limit}&convert=${defaultConvert}`)
  }, [])

  const renderLoadingErrorSuccess = () => {
    if (error) {
      return <div>Error: {error.message}</div>
    } else if (loading) {
      return <div>Loading...</div>
    } else if (cryptoCurrencies?.data) {
        return <Table />
    } else {
      return null
    }
  }

  return (
    <div className='main-wrapper'>
      <div className='main-top'>
        <Header title='Market Watch' />
        <Dropdown options={convert} />
      </div>
      {renderLoadingErrorSuccess()}
    </div>
  )
}

export default Dashboard
