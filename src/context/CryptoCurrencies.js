import React, { createContext, useState } from 'react'

// Initialize the context.
export const CryptoCurrenciesContext = createContext()

export default ({ children }) => {
  // Set the default state.
  const [cryptoCurrencies, setCryptoCurrencies] = useState({})
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)

  // Default values for the context.
  const defaultContext = {
    cryptoCurrencies,
    setCryptoCurrencies,
    loading,
    setLoading,
    error,
    setError
  }

  return (
    <CryptoCurrenciesContext.Provider value={defaultContext}>
      {children}
    </CryptoCurrenciesContext.Provider>
  )
}
