import React from 'react'
import { Route, Switch } from 'react-router-dom'

// Import the routes.
import routes from '../data/routes'

// Views
import Error from '../views/Error'

// Import the error message from the config file.
import Messages from '../config/messages.json'

const Page404 = () => (
  <>
    <Error title='404' message={Messages['404']} />
  </>
)

const Routes = () => (
  <Switch>
    {routes.map((route, index) => (
      <Route
        key={index}
        path={route.path}
        exact={route.exact}
        render={props => (
          <route.layout {...props}>
            <route.component {...props} />
          </route.layout>
        )}
      />
    ))}
    <Route component={Page404} />
  </Switch>
)

export default Routes
