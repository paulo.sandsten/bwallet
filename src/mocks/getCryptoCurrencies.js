import DATA from '../data/mocks/cryptoCurrencies.json'

/**
 * Will get the mocked data from cryptoCurrencies.
 *
 * @returns {Promise<string[]>}
 */
function getCryptoCurrencies () {
  return new Promise((resolve, reject) => {
    resolve(DATA)
  })
}

export { getCryptoCurrencies }
