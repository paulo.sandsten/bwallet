/**
{
  "path": "Path parameter",
  "httpMethod": "Incoming request's method name"
  "headers": {Incoming request headers}
  "queryStringParameters": {query string parameters }
  "body": "A JSON string of the request payload."
  "isBase64Encoded": "A boolean flag to indicate if the applicable request payload is Base64-encode"
}
 */
const rp = require('request-promise')
const API_ENDPOINT = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
const { API_KEY } = process.env

exports.handler = async (event, context) => {
  // Get the query values.
  const start = 1
  const limit = event.queryStringParameters.limit
  const convert = event.queryStringParameters.convert

  // Set the request options.
  const requestOptions = {
    method: 'GET',
    uri: API_ENDPOINT,
    qs: {
      start: start,
      limit: limit,
      convert: convert
    },
    headers: {
      'X-CMC_PRO_API_KEY': API_KEY
    },
    json: true,
    gzip: true
  }

  // Request.
  try {
    const response = await rp(API_ENDPOINT, requestOptions)
    return {
      statusCode: 200,
      body: JSON.stringify(response)
    }
  } catch (error) {
    return { statusCode: 422, body: String(error) }
  }
}
