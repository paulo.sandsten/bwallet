import { useContext } from 'react'

// Import context.
import { CryptoCurrenciesContext } from '../context/CryptoCurrencies'

// Import the Mock API.
import { getCryptoCurrencies } from '../mocks/getCryptoCurrencies'

/**
 * Utility function (custom hook) for fetching data and setting it to the
 * Context state.
 */
const useFetch = () => {
  const { setCryptoCurrencies } = useContext(CryptoCurrenciesContext)
  const { setError } = useContext(CryptoCurrenciesContext)
  const { setLoading } = useContext(CryptoCurrenciesContext)

  /**
   * @param {string} url - the url to fetch the data from.
   * @param {string} options - the request options object for the Fetch API.
   */
  const fetchData = async (url, options = {}) => {
    setLoading(true)
    setError(false)
    // For local development we set a timeout to simulate the network request.
    if (process.env.REACT_APP_ENV === 'local') {
      setTimeout(() => {
        getCryptoCurrencies()
          .then(res => {
            setCryptoCurrencies(res)
            setLoading(false)
          })
      }, 500)
    } else {
      try {
        const res = await fetch(url, options)
        const data = await res.json()
        await console.log(data)
        setCryptoCurrencies(data)
        setLoading(false)
      } catch (error) {
        setError(error)
      }
    }
  }

  return fetchData
}

export default useFetch
