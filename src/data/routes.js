import React from 'react'
import { Redirect } from 'react-router-dom'

// Layouts
import DefaultLayout from '../layouts/Default'

// Views
import Dashboard from '../views/Dashboard'

export default [
  {
    path: '/',
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to='/dashboard' />
  },
  {
    path: '/dashboard',
    exact: true,
    layout: DefaultLayout,
    component: Dashboard
  }
]
