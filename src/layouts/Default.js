import React from 'react'
import PropTypes from 'prop-types'

// Components
import Sidebar from '../components/layouts/Sidebar/Sidebar'

// Styling.
const layoutStyle = {
  display: 'flex'
}

const mainLayoutStyle = {
  marginLeft: '5rem',
  width: '100%',
  height: '100vh',
  overflowY: 'scroll'
}

const DefaultLayout = ({ children, navigation = true, sidebar = true }) => (
  <div className='default-wrapper' style={layoutStyle}>
    <Sidebar />
    <main style={mainLayoutStyle}>{children}</main>
  </div>
)

DefaultLayout.propTypes = {
  navbar: PropTypes.bool,
  sidebar: PropTypes.bool
}

export default DefaultLayout
