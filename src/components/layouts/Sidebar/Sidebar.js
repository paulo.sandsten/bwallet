import React from 'react'

// Import Material Design icons.
import HomeIcon from '@material-ui/icons/Home'
import TrendingUpIcon from '@material-ui/icons/TrendingUp'
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer'
import PeopleIcon from '@material-ui/icons/People'
import SettingsIcon from '@material-ui/icons/Settings'
import NotificationsIcon from '@material-ui/icons/Notifications'

// Import styling.
import './Sidebar.css'

// Import crypto currency logo.
import logo from '../../../images/crypto_currency_logo.svg'

function Sidebar () {
  return (
    <div className='sidebar'>
      <div className='sidebar-logo-wrapper'>
        <img src={logo} alt='crypto currency logo' />
      </div>
      <nav>
        <ul>
          <li><a href='/' data-link='home'><HomeIcon /></a></li>
          <li><a href='/' data-link='market_watch'><TrendingUpIcon /></a></li>
          <li><a href='/' data-link='messages'><QuestionAnswerIcon /></a></li>
          <li><a href='/' data-link='contacts'><PeopleIcon /></a></li>
          <li><a href='/' data-link='notifications'><NotificationsIcon /></a></li>
          <li><a href='/' data-link='settings'><SettingsIcon /></a></li>
        </ul>
      </nav>
    </div>
  )
}

export default Sidebar
