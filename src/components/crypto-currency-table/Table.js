import React from 'react'
import TableRows from './TableRows'

// Import styling.
import './Table.css'

export default () => {
  return (
    <div className='main-table-container'>
      <table>
        <thead>
          <tr>
            <th>Rank</th>
            <th>Name</th>
            <th>Ticker</th>
            <th>Market Cap</th>
            <th>Price</th>
            <th>Volume (24h)</th>
            <th>Circulating Supply</th>
            <th>Change 1h</th>
            <th>Mineable</th>
          </tr>
        </thead>
        <tbody>
          <TableRows />
        </tbody>
      </table>
    </div>
  )
}
