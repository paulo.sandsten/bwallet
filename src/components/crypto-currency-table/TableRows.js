import React, { useContext } from 'react'

// Import the Context.
import { CryptoCurrenciesContext } from '../../context/CryptoCurrencies'

export default () => {
  const { cryptoCurrencies } = useContext(CryptoCurrenciesContext)
  const currency = Object.keys(cryptoCurrencies.data[0].quote)[0]

  /**
   * Parses the amount:
   * - Gives it two decimals
   * - Commas dividing every thousand(s) with toLocaleString()
   * - Gives it a dollar sign as the prefix or the currency as a suffix.
   * 
   * @param {Object} item - The currency object.
   * @param {*} key - What key we will show the value from.
   */
  const parseCurrency = (item, key) => {
    let currencyQuoteSymbol
    if (currency === 'USD') {
      currencyQuoteSymbol = '$'
      return `${currencyQuoteSymbol} ${parseFloat(item.quote[currency][key].toFixed(2)).toLocaleString()}`
    } else {
      return `${parseFloat(item.quote[currency][key].toFixed(2)).toLocaleString()} ${currency}`
    }
  }

  return cryptoCurrencies.data.map((item, index) => {
    // Extra styling for the data when the change is wither positive or negative.
    let textColor
    if (Math.sign(item.quote[currency].percent_change_1h) < 0) {
      textColor = {
        color: '#e03169'
      }
    } else {
      textColor = {
        color: '#d3ffdf'
      }
    }

    return (
      <tr key={index}>
        <td>{item.cmc_rank}</td>
        <td>{item.name}</td>
        <td>{item.symbol}</td>
        <td>{parseCurrency(item, 'market_cap')}</td>
        <td>{parseCurrency(item, 'price')}</td>
        <td>{parseCurrency(item, 'volume_24h')}</td>
        <td>{Math.floor(item.total_supply).toLocaleString() + ' ' + item.symbol}</td>
        <td style={textColor}>{item.quote[currency].percent_change_1h}</td>
        {item.tags?.includes("mineable") ? <td>Yes</td> : <td>No</td>}
      </tr>
    )
  })
}
