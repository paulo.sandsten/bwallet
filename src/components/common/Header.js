import React from 'react'
import PropTypes from 'prop-types'

// Import styling.
import './Header.css'

/**
 * Functional component for the common header.
 */
const Header = (props) => {
  return (
    <div className='main-header'>
      <p>Welcome, {props.name}</p>
      <h2>{props.title}</h2>
    </div>
  )
}

Header.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string
}

Header.defaultProps = {
  name: 'John',
  title: 'Custom Header Title'
}

export default Header
