import React from 'react'
import useFetch from '../../utils/UseFetch'

// Import custom styling.
import './Dropdown.css'

export default ({ options }) => {
  const fetchData = useFetch()
  const start = 1
  const limit = 25
  // TODO: Fix this to be differect for localhost. and in a config file.
  const baseUrl = '/.netlify/functions/cryptocurrencies'

  return (
    <select
      className='currency-selector'
      onChange={e => {
        fetchData(`${baseUrl}?start${start}?limit=${limit}&convert=${e.target.value}`)
      }}
    >
      {options.map((o, index) => (
        <option key={index} value={o}>{o}</option>
      ))}
    </select>
  )
}
